data = load('Cars.mat');
filePaths = data.gTruth.DataSource.Source;
trainingBoxes = data.gTruth.LabelData;
plates = [filePaths,trainingBoxes];

I = imread(plates.Var1{1});
I = insertObjectAnnotation(I,'Rectangle',plates.plate{1},'vehicle plate','LineWidth',8);


 options = trainingOptions('sgdm', ...
        'MiniBatchSize', 4, ...
        'InitialLearnRate', 1e-3, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.1, ...
        'LearnRateDropPeriod', 100, ...
        'MaxEpochs', 30, ...
        'Verbose', true);
        
    %load('cifar.mat')
    network = alexnet;
    layersToTransfer = network.Layers(1:end-3);
    
    layers = [
    layersToTransfer
    fullyConnectedLayer(2,'WeightLearnRateFactor',20,'BiasLearnRateFactor',20)
    softmaxLayer
    classificationLayer];

    rcnnDetector = trainRCNNObjectDetector(plates, layers, options, ...
    'NegativeOverlapRange', [0 0.5], 'PositiveOverlapRange',[0.8 1])
     save('alexnetDetector.mat','rcnnDetector');
     
     