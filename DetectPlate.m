function [x,outputImage] =  DetectPlate (filename)
load('AlexnetDetector.mat'); 
%testImage = imread('rotated.png');
testImage = imread(filename);
testImage = imresize(testImage,[800,600]);
[bboxes,score,label] = detect(fcnn,testImage);                              %#ok<ASGLU>

outputImage = insertShape(testImage,'Rectangle',bboxes,'LineWidth',3);
for i = 1 : numel(label)
   bbox = bboxes(i,:);
   outputImage = insertText(outputImage,[bboxes(i,1) + bboxes(i,3)...
       bboxes(i,2)-20],'plate','FontSize',10,'TextColor','red');
   im2 = imcrop(testImage,bbox);
   name = strcat('vehicle_plate',num2str(i),'.jpg');
   imwrite(im2,name);
end
 
%figure
%imshow(outputImage)

imwrite(im2,'vehicle_plate1.jpg');
x = name;

end







