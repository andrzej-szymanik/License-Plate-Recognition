%data  = load('newCars3.mat');
data = load('newCars.mat');
filePaths = data.gTruth.DataSource.Source;
trainingBoxes = data.gTruth.LabelData;
table = [filePaths,trainingBoxes]
plates = table(1:200,:);
load('newrcnn.mat');
    results = zeros(1,5);

   % for i=1:200
    %value = struct2cell(data.gTruth.LabelData.plate{i});
    %plates.plate{i} = value{1};
%end
for i=1:200
 testImage = imread(plates.Var1{i});
      [bboxes,score,label] = detect(fcnn,testImage);
        [score, idx] = max(score);
        bbox = bboxes(idx,:);
        
        if ~isempty(bbox)
            [filepath,name,ext] = fileparts(plates.Var1{i})
            im2 = imcrop(testImage,bbox);
            expectedText = convertCharsToStrings(name)
            fileName2 = strcat('recognizedPlate',num2str(i))
            imwrite(im2,strcat(fileName2,'.jpg'));
            overlapRatio = max(bboxOverlapRatio(bboxes,plates.plate{i}));
            ocrText = detectText(strcat(fileName2,'.jpg'));
            distance = max(editDistance(ocrText,expectedText));
            if isempty(ocrText)
               ocrText = ""; 
            end
            row = [i,overlapRatio, expectedText,distance,ocrText];
             results = [results;row]
        
        else
             row = [i,0, expectedText,"Wrong","Failed"];
             results = [results;row]
        end


end