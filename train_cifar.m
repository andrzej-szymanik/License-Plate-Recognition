cifar10Data = tempdir;
url = 'https://www.cs.toronto.edu/~kriz/cifar-10-matlab.tar.gz';
Cifar10Data.download(url, cifar10Data);

[trainingImages, trainingLabels, testImages, testLabels] = Cifar10Data.load(cifar10Data);
numberofClasses = 10;

[height, width, numChannels, ~] = size(trainingImages);
sizeOfImage = [height, width, numChannels];
inputLayer = imageInputLayer (sizeOfImage)

filterSize = [5 5 ];
numberOfFilters = 32;

middleLayers = [ 
    convolution2dLayer(filterSize, numberOfFilters, 'Padding', 2)
    reluLayer()
    maxPooling2dLayer(3,'Stride',2)
    convolution2dLayer(filterSize, numberOfFilters, 'Padding', 2)
    reluLayer()
    maxPooling2dLayer(3,'Stride',2)
    convolution2dLayer(filterSize,2* numberOfFilters, 'Padding', 2)   
    reluLayer()
    maxPooling2dLayer(3,'Stride',2)]

finalLayers = [
    fullyConnectedLayer(64)
    reluLayer
    fullyConnectedLayer(numberofClasses)
    softmaxLayer
    classificationLayer]

layers = [
    inputLayer
    middleLayers
    finalLayers]

layers(2).Weights = 0.0001 * randn([filterSize numChannels numberOfFilters])
options = trainingOptions('sgdm', ...
    'Momentum', 0.8, ...
    'InitialLearnRate', 0.001, ...
    'LearnRateSchedule', 'piecewise', ...
    'LearnRateDropFactor', 0.1, ...
    'LearnRateDropPeriod', 8, ...
    'L2Regularization', 0.004, ...
    'MaxEpochs', 100, ...
    'MiniBatchSize', 128, ...
    'Verbose', true);

    cifar10Network = trainNetwork(trainingImages, trainingLabels, layers, options);
   
