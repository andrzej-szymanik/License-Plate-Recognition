function x = detectText(filename)
%function x = detectText(filename)
resImage = imread(filename);
%resImage = imread('recognizedPlate14.jpg');
sizeOfImage = size(resImage);
ratio = 400/sizeOfImage(2)
resImage = imresize(resImage,ratio);



grayImage = rgb2gray(resImage);

%imshow(grayImage)

level = graythresh(grayImage);
BW = imbinarize(grayImage,level);
BW = ~BW;

SE = strel('disk',3');

s = regionprops(BW, 'BoundingBox');
bb = round(reshape([s.BoundingBox], 4, []).');


%Remove bboxes above ratio
for idx = numel(s):-1:1
    bbox = bb(idx,:);
    ratio = bbox(3) / bbox(4);
    if ratio > 1.2
        s(idx) = [];
        bb(idx,:) = [];
    end

end


%Compute average area
area = [];
for idx = 1:numel(s)
    bbox = bb(idx,:);
    area(idx) = bbox(3) * bbox(4)
end
maxArea = max(area);
for idx = numel(s) : -1 : 1
    if 4 * area(idx) < maxArea
        s(idx) = [];
        bb(idx,:) = [];
    end
end

leftBbox = s(1).BoundingBox;
rightBbox = s(numel(s)).BoundingBox;
angle = atan2(rightBbox(2)-leftBbox(2),rightBbox(1)-leftBbox(1));
BW = imrotate(BW,angle*180/pi);



CC = bwconncomp(BW,8)
minimum = min(bb(:,2));
maximum = max(bb(:,2)+ bb(4));
imageSize = size(resImage);
for i =minimum :-1: 1
    BW(i,:) = 0;
end
for i = maximum:imageSize(1)
   BW(i,:) = 0; 
end
imshow(BW);

for i = 1:numel(CC.PixelIdxList)
    numPixels = cellfun(@numel,CC.PixelIdxList);
        
  
 if(numPixels(i) < 200 || numPixels(i) > 2000)
  
 BW(CC.PixelIdxList{i}) = 0;
        end
end
imshow(BW)

s = regionprops(BW, 'BoundingBox');
bb = round(reshape([s.BoundingBox], 4, []).');

for(idx = 1: numel(s))
    rectangle('Position', bb(idx,:), 'edgecolor', 'red');
    bboxA = bb(idx,:);
    bbox = imcrop(BW,bboxA);
end

numeryRejestracyjne = "";

for idx = 1 : numel(s)
    filteredCharacters ="";
    bboxA = bb(idx,:);
    bbox = imcrop(BW,bboxA);
    %ocrResult = ocr(bbox,'Language', '/home/andrzej/Inzynierka - MATLAB/Processed Plates/myLang/tessdata/eng2.traineddata');
    ocrResult = ocr(bbox,'TextLayout','Block');
    character = ocrResult.Text;
        isLower = isstrprop(ocrResult.Text,'lower');
       
    if contains(character,'ii')
        character = '1';
    end
    if(contains(character,'%'))
        character = '8';
    end
            
    TF = isstrprop(character,'digit');
    YF = isstrprop(character,'alpha');
    alphaDigit = TF | YF;
    for i = 1 : numel(character)
        if alphaDigit(i) == 1
            filteredCharacters = strcat(filteredCharacters,character(i));
        end
    end
    if strlength(filteredCharacters) > 0 
    numeryRejestracyjne = strcat(numeryRejestracyjne,filteredCharacters(1));
   end
    
    end
 




%imshowpair(resImage,BW,'montage')
word = numeryRejestracyjne{1};
numeryRejestracyjne = convertCharsToStrings(word);
x = numeryRejestracyjne;
end



